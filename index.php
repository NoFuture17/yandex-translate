<?php

require_once 'autoload.php';

use nofuture17\Translate\Translator;
use nofuture17\Translate\Exception;

$key = [
    'trnsl.1.1.20160326T110427Z.3fe87a81777e03b3.4af1041bb003e5426170d99ea909d9f64280b5fb',
    'trnsl.1.1.20160307T043951Z.eb98b0156de6283b.056e874021be380796d93cd8907c3095a2e280fd'
];

try {
    $translator = new Translator($key);
    //print_r($translator->getSupportedLanguages('ru'));

    $translation = $translator->translate('Hello world', 'en-ru');
    echo $translation . PHP_EOL; // Привет мир

    $translation = $translator->translate('Hello all world', 'en-ru');
    echo $translation . PHP_EOL; // Привет мир

    $translation = $translator->translate('Hello not all world', 'en-ru');
    echo $translation . PHP_EOL; // Привет мир

    $translation = $translator->translate('Привет, почти весь, мир!', 'ru-en');
    echo $translation . PHP_EOL; // Привет мир

    //echo $translation->getSource(); // Hello world;

    //echo $translation->getSourceLanguage(); // en
    //echo $translation->getResultLanguage(); // ru
} catch (Exception $e) {
    // handle exception
}